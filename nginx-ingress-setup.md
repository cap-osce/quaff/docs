# Setting up NGINX Ingress

## Requirements

- `kubectl` - connected to your cluster.
- `helm`

## Installation

Install `tiller` on the cluster:

```bash
helm init
```

Setup the default kube-system service account as a cluster admin, this allows tiller to deploy things:

```bash
kubectl create clusterrolebinding add-on-cluster-admin --clusterrole=cluster-admin --serviceaccount=kube-system:default
```

Install the NGINX Ingress Controller

```bash
helm install --name nginx --set rbac.create=true stable/nginx-ingress
```

In any `ingress.yaml` files for any applications you make, ensure that under `metadata > annotations`, you define the `ingress.class` as `"nginx"`.  
For example:

```yaml
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: example-app
  namespace: example-namespace
  labels:
    app: example-app
  annotations:
    kubernetes.io/ingress.class: "nginx"
```